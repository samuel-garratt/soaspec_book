# Dependencies
## Install Ruby
There are many tutorials online for doing this. Search for the appropriate one for
your operating system.
## Install `bundler`
On the command line type
```
gem install bundler
```

## Install `soaspec`
```
gem install soaspec
```
Please ensure this is the latest version. If you have an existing version
```
gem update soaspec
```
to ensure you are up to date with the latest one.

Other gems will be introduced in this book. By installing `Soaspec` you will install
many dependent gems, e.g, `rest-client`.

## Install a version control system 
Yes this tutorial encourages you to version control your code. The idea is that
version control and running tests via CI systems be practiced early on.
`git` is used in tutorials though you can follow the commands with an equivalent if
another technology is desired.

## Create a Gitlab account
To help easily understand the concept of CI, this book encourages `GitLab` as the
medium and will give examples of `.gitlab-ci.yml` files to run your test code in
a container. It should be easy to transfer configuration to any other CI system.
Creating a Gitlab account and use of it's pipelines is free forever so don't be 
afraid to give it a try.