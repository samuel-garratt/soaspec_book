This page explains how to perform your first GET. GET requests will be used in 
the initial examples. Then `POSTS` and other actions will be described later.

On the command prompt type `restclient`. This will start
an IRB (Interactive Ruby) session with the `rest-client` gem already required. This
allows you to type ruby commands and see their response after pressing enter.

Now on the first line type
```ruby
response = RestClient.get 'https://samuel-garratt.gitlab.io/soaspec/example_responses/basic.json'
```
This will make a `GET` request to the URL `https://samuel-garratt.gitlab.io/soaspec/example_responses/basic.json`
and store the response in a local variable called `response`.

From this `response` object, various parts of the response can be obtained.
Perform the following to interrogate the response.

```ruby
response.public_methods   # See list of all methods available on response object
response.body             # See body of response
response.code             # See HTTP status code
```

See the gem's [home page](https://github.com/rest-client/rest-client) for more 
details on what can be done.

Type `exit` to exit the `IRB` terminal that opened.