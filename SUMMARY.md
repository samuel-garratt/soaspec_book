# Summary

## Getting started
* [Introduction](README.md)
* [Dependencies](DEPENDENCIES.md)
* [Doing your first Get](DoingFirstGet.md)
* [Running API from CI](RunningApiFromCI.md)
* [Cucumber and REST](GettingStartedCucumberRest.md)

