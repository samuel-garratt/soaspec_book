Feature: Houses

  Scenario: Retrieve the name of the first house
    Given I am performing a get on the 'Houses' API
    And I use the path 'basic.json'
    When I make the request
    Then it should be successful
    And it should have the name "Bob's house"