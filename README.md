# Soaspec - using objects to simplify API testing

Welcome to this book on [Soaspec](https://gitlab.com/samuel-garratt/soaspec). Soaspec is a ruby `gem` made to help with defining
common settings for an `API` that is to be used as part of a automated test suite.

Note: This book is a work in progress. Only just started. Much more structure and content
will be added.

This book covers some of the basics of Ruby so the initial sections may be too wordy 
for someone with prior Ruby experience.

Ensure you have met the [dependencies](DEPENDENCIES.md) before walking through the
examples in the book. It is designed that one go through this book in order as 
concepts will be explained in detail first and then assumed to be understood later
on.