This page explains how to get started creating a suite in `Soaspec` for a `REST`
api using generic step definitions. 

# Create initial folder and files

Run `soaspec cucumber` within your repository. 

This will create a number of files automatically which will be described below 

## Gemfile
The required dependencies are in a file called `Gemfile` at the root of 
your directories, having at least the following gems

```ruby
source 'https://rubygems.org'

gem 'cucumber'    # Used for the testing framework
gem 'require_all' # Used to make loading the library folder easy
gem 'soaspec'     # Handles working with the API
```

## Features

This folder will contain your [cucumber](https://cucumber.io) features. Cucumber features
files all end in `.feature`. Within this folder are two special folders:

### support

The `support` folder has files that will be loaded before any cucumber features are
written. Within it there is an `env.rb` that loads the required files for setting
up the environment for testing. By default it requires all files located within the
`lib` folder of your project.

### step_definitions

The `step_definitions` folder contains all the code with the definitions of what code
`Cucumber` should execute when it finds a step. Within this, a `generic_steps.rb` file
is created which contains `generic` code you can use to easily get started performing actions
on the API using Soaspec.

# Install dependencies

On the command line install the dependencies for this project by typing `bundle install`.

# Create a class for your API

Within the `lib` folder, create a class inheriting from `Soaspec::RestHandler` to
define how to interact with your API. You could do this manually. However here we
will use the `soaspec generate` command to help us.

After entering `soaspec generate` on the command line you should see a browser open up
where in you can put the details of the `Handler` that will be created.  

In the class name field put `Houses`, for the base url put 
`https://samuel-garratt.gitlab.io/soaspec/example_responses` and then click the
`Generate Handler` button. You should see a confirmation that your file has been
created.
 
Close the browser and stop the process.

# Create a cucumber feature that tests an API

Within the `features` folder add a file called `houses.feature` and the 
following code to test this API.

```gherkin
Feature: Houses

  Scenario: Retrieve the name of the first house
    Given I am performing a get on the 'Houses' API
    And I use the path 'basic.json'
    When I make the request
    Then it should be successful
    And it should have the name "Bob's house"
```

Run the code by executing `cucumber` by typing `cucumber` on the command line.
Note that you did not have to implement any of these step definitions. They made
use of the generic step definitions. 