# This class represent REST Api calls for the Houses API
class Houses < Soaspec::RestHandler
  ## Defining request

  # All requests to Houses will start with this url 
  base_url 'https://samuel-garratt.gitlab.io/soaspec/example_responses'
  
  # Headers that will be sent by default using this Handler
  # If symbol is used, they'll be converted to standard HTTP headers
  # headers accept: 'application/json', content_type: 'application/json'

  # Filename of oauth2 file to use for oauth2 parameters. 'Soaspec.credentials_folder' can be set to globally defined a folder with credentials
  # This will add a default 'Authorization: Bearer access_token' header if 'Authorization' is not specified in headers
  # The '.yml' extension will be added automatically if not present.
  # oauth2_file 'filename.yml'

  
  # Filename of YAML with basic auth parameters in it 'Soaspec.credentials_folder' can be set to globally defined a folder with credentials
  # It is of the format
  # user: 'username'
  # password: 'password'
  # basic_auth_file 'basic_auth.yml' # Load YAML file with basic auth credentials in it
  

  ### Request Body

  # Filename of template to be used. This is within 'Soaspec.template_folder' folder
  # template_name 'filename'

  # Default hash to be used in request that will be converted to JSON. This can be overriden and added to by each Exchange using this class
  # default_hash a: 1, b: 2

  ## Extracting response

  # Use this to extract the value of either a JSON (JSONPath) or XML (Xpath) element. 'element_name' is the method that will be
  # generated on an Exchange to obtain it
  # element :element_name, 'element_path'

  # Use this to extract an attribute from XML. If the name of the method and the attribute name are the same then only one parameter is
  # needed
  # attribute(:attribute_method_name, 'name_of_attribute')

  ## Creating verifications for success response

  # Values that must have a certain value for 'success scenario' shared example
  # mandatory_json_values '$..status' => 'available'

  # Elements that must be in the response for 'success scenario' shared example
  # Below will expect an element at the path 'tags' to be present
  # mandatory_elements :tags
end
